﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PasswordChecker;
using PasswordChecker.Checkers;

namespace PasswordCheckerTest
{
    [TestClass]
    public class CheckPasswordTest
    {
        private IPasswordChecker _passwordChecker;
            
        [TestInitialize]
        public void TestInitialize()
        {
            _passwordChecker = new PasswordChecker.PasswordChecker();
        }

        [TestMethod]
        public void VerifyTest_If_Password_Length_Less_Than7_Chars_For_User_ReturnFalse()
        {
            var testPassword = "abcd";
            bool expected = false;
            var isAdmin = false;
            var actual=_passwordChecker.Verify(testPassword,isAdmin);
            Assert.AreEqual(expected,actual.Item1);
        }


        [TestMethod]
        public void VerifyTest_If_Password_Does_Not_Contains_At_Least_One_Alphabetical_Character_ReturnFalse()
        {
            var testPassword = "trifim";
            bool expected = false;
            var isAdmin = false;
            var actual = _passwordChecker.Verify(testPassword, isAdmin);
            Assert.AreEqual(expected, actual.Item1);
        }


        [TestMethod]
        public void VerifyTest_If_Password_Does_Not_Contains_At_Least_One_Digit_Character_ReturnFalse()
        {
            var testPassword = "trifiM";
            bool expected = false;
            var isAdmin = false;
            var actual = _passwordChecker.Verify(testPassword,isAdmin);
            Assert.AreEqual(expected, actual.Item1);
        }

        [TestMethod]
        public void VerifyTest_If_Password_Length_Less_Than10_Chars_For_Admin_ReturnFalse()
        {
            var testPassword = "abcd";
            bool expected = false;
            var isAdmin = false;
            var actual = _passwordChecker.Verify(testPassword, isAdmin);
            Assert.AreEqual(expected, actual.Item1);
        }


        [TestMethod]
        public void VerifyTest_If_Password_Does_Not_Contains_At_Least_One_Special_Character_Only_For_Admin_ReturnFalse()
        {
            var testPassword = "trofim";
            bool expected = false;
            var isAdmin = false;
            var actual = _passwordChecker.Verify(testPassword,isAdmin);
            Assert.AreEqual(expected, actual.Item1);
        }


        [TestMethod]
        public void VerifyTest_If_Password_Length_More_Than7_Contains_At_Least_One_Digit_One_Alphabetical_Characterss_For_User_ReturnTrue()
        {
            var testPassword = "ada1vcdsD";
            bool expected = true;
            var isAdmin = false;
            var actual = _passwordChecker.Verify(testPassword,isAdmin);
            Assert.AreEqual(expected, actual.Item1);
        }

        [TestMethod]
        public void VerifyTest_If_Password_Length_More_Than10_Contains_At_Least_One_Digit_One_Alphabetical_And_Special_Characters_For_Admin_ReturnTrue()
        {
            var testPassword = "abcda1v~cdsD";
            bool expected = true;
            var isAdmin = true;
            var actual = _passwordChecker.Verify(testPassword,isAdmin);
            Assert.AreEqual(expected, actual.Item1);
        }
       
    }
}

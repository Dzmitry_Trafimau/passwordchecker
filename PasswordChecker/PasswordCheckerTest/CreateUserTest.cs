﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PasswordChecker;
using PasswordChecker.Repositories;
using PasswordChecker.Repositories.Interfaces;
using Rhino.Mocks;

namespace PasswordCheckerTest
{
    [TestClass]
    public class CreateUserTest
    {
        private IPasswordChecker _passwordChecker;
        private IUserRepository _userRepository;
        private string _password = "supersS25";
        private bool _isAdmin;
        private string _userName;

        [TestInitialize]
        public void InitializeTest()
        {
            _userName = "dzmitry";
            _password = "supersS25";
            _isAdmin = false;
            var returnValue = new Tuple<bool, List<string>>(true, null);

            _passwordChecker = MockRepository.GenerateStub<IPasswordChecker>();
            _passwordChecker.Stub(
                  _ => _.Verify(_password, _isAdmin))
                  .Return(returnValue);

            _userRepository = new UserRepository(_passwordChecker);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Error. You cannot create an user second time.")]
        public void CreateUserTest_If_it_will_be_called_2_Times_returnExeption()
        {
            _userRepository.CreateUser(_userName, _password, _isAdmin);
            _userRepository.CreateUser(_userName, _password, _isAdmin);
        }

        [TestMethod]
        public void CreateUserTest_If_it_will_be_called_1_Times_returnIntValue()
        {
            var actual = _userRepository.CreateUser(_userName, _password, _isAdmin);
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual > 0 && actual <= 1000);
        }
    }
}

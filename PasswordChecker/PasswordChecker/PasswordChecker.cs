﻿using System;
using System.Collections.Generic;
using PasswordChecker.Checkers;

namespace PasswordChecker
{
    public class PasswordChecker : IPasswordChecker
    {
        public Tuple<bool, List<string>> Verify(string password, bool isAdmin)
        {
            ICheckValue checkValue = new CheckValue(password);
            var listCheckers = new List<Func<Tuple<bool, string>>>
            {
                checkValue.IsPasswordContainsAnAlphabeticalCharacter,
                checkValue.IsPasswordContainsADigitCharacter
            };
            if (isAdmin)
            {
                listCheckers.Add(checkValue.IsPasswordLengthdMoreThan10);
                listCheckers.Add(checkValue.IsPasswordContainsSpecialCharacter);
            }
            else
                listCheckers.Add(checkValue.IsPasswordLengthdMoreThan7);

            return CheckErrorListAndCreateResult(listCheckers);
        }

        private Tuple<bool, List<string>> CheckErrorListAndCreateResult(IEnumerable<Func<Tuple<bool, string>>> allCheckers)
        {
            var errorList = new List<string>();
            var isCorrectPassword = true;
            foreach (var checker in allCheckers)
            {
                var checkerResult = checker.Invoke();
                if (!checkerResult.Item1)
                {
                    isCorrectPassword = false;
                    errorList.Add(checkerResult.Item2);
                }
            }
            return new Tuple<bool, List<string>>(isCorrectPassword, errorList);
        }
    }
}
﻿using System;
using System.Linq;

namespace PasswordChecker.Checkers
{
    public class CheckValue:ICheckValue
    {
        private string _password;

        public CheckValue(string password)
        {
            _password = password;
        }

        private Tuple<bool, string> IsPasswordLengthdMoreThanMinvalue(int minLength, string password)
        {
            return password.Length > minLength ? new Tuple<bool, string>(true, "Ok") :
                new Tuple<bool, string>(false, "Your password less than " + minLength + " symbols");

        }

        public Tuple<bool, string> IsPasswordLengthdMoreThan7()
        {
            return IsPasswordLengthdMoreThanMinvalue(7, _password);
        }

        public Tuple<bool, string> IsPasswordLengthdMoreThan10()
        {
            return IsPasswordLengthdMoreThanMinvalue(10, _password);
        }

        public Tuple<bool, string> IsPasswordContainsAnAlphabeticalCharacter()
        {
            if (_password.Any(Char.IsLetter)) return new Tuple<bool, string>(true, "Ok");
            return new Tuple<bool, string>(false, "Your password doesn't contains at least one alphabetical character");
        }

        public Tuple<bool, string> IsPasswordContainsADigitCharacter()
        {
            if (_password.Any(Char.IsDigit)) return new Tuple<bool, string>(true, "Ok");
            return new Tuple<bool, string>(false, "Your password doesn't contains at least one digit character");
        }

        public Tuple<bool, string> IsPasswordContainsSpecialCharacter()
        {
            if (_password.Any(_ => _.Equals('~'))) return new Tuple<bool, string>(true, "Ok");
            return new Tuple<bool, string>(false, "Your password doesn't contains special character");
        }
    }
}
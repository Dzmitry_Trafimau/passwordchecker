﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordChecker.Checkers
{
    public interface ICheckValue
    {
        Tuple<bool, string> IsPasswordLengthdMoreThan7();

        Tuple<bool, string> IsPasswordLengthdMoreThan10();

        Tuple<bool, string> IsPasswordContainsAnAlphabeticalCharacter();

        Tuple<bool, string> IsPasswordContainsADigitCharacter();

        Tuple<bool, string> IsPasswordContainsSpecialCharacter();
    }
}

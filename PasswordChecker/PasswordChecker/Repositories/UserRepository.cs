﻿using System;
using System.Collections.Generic;
using System.Linq;
using PasswordChecker.Repositories.Interfaces;
using Rhino.Mocks;

namespace PasswordChecker.Repositories
{
    public class UserRepository:IUserRepository
    {
        private IUserRepository _userRepository;
        private IPasswordChecker _passwordChecker;

        public UserRepository(IPasswordChecker passwordChecker)
        {
            _userRepository = MockRepository.GenerateMock<IUserRepository>();
            _userRepository.Expect(
                _ => _.CreateUser(Arg<string>.Is.Anything, Arg<string>.Is.Anything, Arg<bool>.Is.Anything))
                .Repeat.Once()
                .Return(new Random().Next(1, 1000));
            _userRepository.Stub(
                _ => _.CreateUser(Arg<string>.Is.Anything, Arg<string>.Is.Anything, Arg<bool>.Is.Anything))
                .Throw(new Exception("Error. You cannot create an user second time."));

            _passwordChecker=passwordChecker;
        }

        public int CreateUser(string userName, string password, bool isAdmin)
        {
            var resultOfCheck = _passwordChecker.Verify(password, isAdmin);
            var isCorrectPassword = resultOfCheck.Item1;
            if (isCorrectPassword)
            {
                var newUserId = CreateMockUser(userName, password, isAdmin);
                return newUserId;
            }
            throw new Exception(GetStringFromListSting(resultOfCheck.Item2));
        }


        private int CreateMockUser(string userName,string password,bool isAdmin)
        {
            return _userRepository.CreateUser(userName, password, isAdmin);
        }

        private string GetStringFromListSting(IEnumerable<string> stringList)
        {
            var result = stringList.Aggregate(string.Empty, (current, message) => current + message);
            return result;
        }
    }
}
﻿namespace PasswordChecker.Repositories.Interfaces
{
    public interface IUserRepository
    {
        int CreateUser(string userName,string password,bool isAdmin);
    }
}

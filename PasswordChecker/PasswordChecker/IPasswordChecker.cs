﻿using System;
using System.Collections.Generic;

namespace PasswordChecker
{
    public interface IPasswordChecker
    {
        Tuple<bool, List<string>> Verify(string password, bool isAdmin);
    }
}
